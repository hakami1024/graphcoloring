﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GraphRepresentation;

namespace Algorithms
{
    public class GraphAlgorithm
    {

        private static readonly Random r = new Random();

        public static int Algorithm( Vertex[] v, LinkedList<Edge>[] e, int len ) {

            bool[,] adjacent = new bool[len,len];

            for( int i = 0; i < len; i++ ){
                if( e[i] == null )
                    continue;

                foreach( Edge edge in e[i] ){
                    int toVertex = edge.toVertex;
                    int fromVertex = i;
                    if( fromVertex > toVertex ){
                        int temp = fromVertex;
                        fromVertex = toVertex;
                        toVertex = temp;
                    }

                    adjacent[ fromVertex, toVertex ] = true;

                    bool checkFirst = true;
                    for( int t = i - 1; t >= 0; t-- ){
                        if( !(adjacent[ t, fromVertex ] || adjacent[ t, toVertex ]) ){
                            if( checkFirst )
                                adjacent[ t, fromVertex ] = true;
                            else
                                adjacent[ t, toVertex ] = true;
                            checkFirst = !checkFirst;
                        }
                    }
                    
                }
            }

            int colorNum = 0;
            for ( int i = 0; i < len; i++ ) {
                Color curColor = Color.FromArgb( r.Next( 255 ), r.Next( 255 ), r.Next( 255 ) );
                
                if ( v[ i ].color != Color.White ) 
                    continue;
                
                for ( int j = i; j < len; j++ ) {
                    if ( !adjacent[i, j] ) {
                        v[ j ].color = curColor;
                    }
                }

                colorNum++;
            }

            return colorNum;
        }
    }
}
