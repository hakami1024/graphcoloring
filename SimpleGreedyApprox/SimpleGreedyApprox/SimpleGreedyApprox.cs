﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GraphRepresentation;

namespace Algorithms
{
    public class GraphAlgorithm
    {
        private static readonly Random random = new Random();

        public static int Algorithm( Vertex[] v, LinkedList<Edge>[] e, int len ) {

            HashSet<int>[] edges = new HashSet<int>[ len ];
            for ( int i = 0; i < len; i++ )
                edges[ i ] = new HashSet<int>();

            for ( int i = 0; i < len; i++ ) {
                if ( e[ i ] == null )
                    break;
                foreach ( Edge edge in e[i] ) {
                    int toVertex =edge.toVertex;
                    edges[ i ].Add( toVertex );
                    edges[ toVertex ].Add( i );
                }
            }

            
            Color[] colors = new Color[ len ];
            for ( int i = 0; i < len; i++ )
                colors[ i ] = Color.FromArgb( random.Next( 255 ), random.Next( 255 ), random.Next( 255 ) );

            v[ 0 ].color = colors[ 0 ];
            int colorNum = 1;

            for ( int i = 1; i < len; i++ ) {
                int curColor = 0;
                foreach ( int toVertex in edges[ i ] )
                    if ( v[ toVertex ].color == colors[ curColor ] )
                        curColor++;
                v[ i ].color = colors[ curColor ];

                if ( curColor >= colorNum )
                    colorNum = curColor + 1;
            }

            return colorNum;
        }
    }
}

