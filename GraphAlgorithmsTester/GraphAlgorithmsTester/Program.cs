﻿using System;
using System.Collections.Generic;
using Algorithms;
using GraphRepresentation;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(){
            Vertex[] vertexes;
            LinkedList< Edge >[] edges;

            init6( out vertexes, out edges  );
           
            int colors = GraphAlgorithm.Algorithm(vertexes, edges, vertexes.Length);
            Console.WriteLine("Colors: " + colors);
            foreach (Vertex vertex in vertexes)
            {
               Console.WriteLine(vertex.name + " - " + vertex.color);
            }
            Console.Read();

        }

        private static void init00(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 )
            };
            LinkedList<Edge>[] edges ={
                null
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init0(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >(new List<Edge>{ new Edge( 2-1, 0 ), new Edge( 3-1, 0 )}),
                new LinkedList< Edge >(), 
                null
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init1(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
                new Vertex( "4", 0, 0 ), 
                new Vertex( "5", 0, 0 ), 
                new Vertex( "6", 0, 0 ), 
                new Vertex( "7", 0, 0 ), 
                new Vertex( "8", 0, 0 )
            };
            LinkedList<Edge>[] edges ={
                /*new LinkedList< Edge >{ new Edge( 2-1, 0 ), new Edge( 3-1, 0 )},
                new LinkedList< Edge >(), 
                new LinkedList< Edge >()*/
                new LinkedList< Edge >( new List< Edge >{new Edge( 2-1, 0 ), new Edge( 3-1, 0 ), new Edge( 6-1, 0 )}),
                new LinkedList< Edge >( new List< Edge >{new Edge( 4-1, 0 ), new Edge( 5-1, 0 )}),
                new LinkedList< Edge >( new List< Edge >{new Edge( 4-1, 0 ), new Edge( 7-1, 0 )}),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >( new List< Edge >{new Edge( 8-1, 0 )}),
                new LinkedList< Edge >()
            };

            verts = vertexes;
            edgs = edges;
        }
        
        private static void init2(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
                new Vertex( "4", 0, 0 ), 
                new Vertex( "5", 0, 0 ), 
                new Vertex( "6", 0, 0 ), 
                new Vertex( "7", 0, 0 )
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >( new List< Edge >{new Edge( 2-1, 0 ), new Edge( 3-1, 0 )}),
                new LinkedList< Edge >( new List< Edge >{new Edge( 3-1, 0 ), new Edge( 7-1, 0 )}),
                new LinkedList< Edge >( new List< Edge >{new Edge( 5-1, 0 ), new Edge( 7-1, 0 )}),
                new LinkedList< Edge >( new List< Edge >{new Edge( 5-1, 0 ), new Edge( 7-1, 0 ), new Edge( 6-1, 0 )}),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >()
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init3(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
                new Vertex( "4", 0, 0 ),
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >( new List< Edge >{new Edge( 2-1, 0 ), new Edge( 3-1, 0 )}),
                new LinkedList< Edge >( ),
                new LinkedList< Edge >( new List< Edge >{new Edge( 4-1, 0 ) }),
                new LinkedList< Edge >( ),
                
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init4(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >(new List<Edge>{ new Edge( 2-1, 0 ), new Edge( 3-1, 0 )}),
                new LinkedList< Edge >(new List<Edge>{ new Edge( 3-1, 0 )}),
                new LinkedList< Edge >()
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init5(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
                new Vertex( "4", 0, 0 ), 
                new Vertex( "5", 0, 0 ), 
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >(new List<Edge>{ new Edge( 2-1, 0 ), new Edge( 3-1, 0 ), new Edge( 4-1, 0 ), new Edge( 5-1, 0 )}),
                new LinkedList< Edge >(new List<Edge>{ new Edge( 3-1, 0 ), new Edge( 4-1, 0 ), new Edge( 5-1, 0 )}),
                new LinkedList< Edge >(new List<Edge>{ new Edge( 4-1, 0 ), new Edge( 5-1, 0 )}),
                new LinkedList< Edge >(new List<Edge>{ new Edge( 5-1, 0 )}),
                new LinkedList< Edge >()
            };

            verts = vertexes;
            edgs = edges;
        }

        private static void init6(out Vertex[] verts, out LinkedList<Edge>[] edgs)
        {
            Vertex[] vertexes ={
                new Vertex( "1", 0, 0 ), 
                new Vertex( "2", 0, 0 ), 
                new Vertex( "3", 0, 0 ), 
                new Vertex( "4", 0, 0 ), 
                new Vertex( "5", 0, 0 ), 
            };
            LinkedList<Edge>[] edges ={
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >(),
                new LinkedList< Edge >()
            };

            verts = vertexes;
            edgs = edges;
        }
    }
}
