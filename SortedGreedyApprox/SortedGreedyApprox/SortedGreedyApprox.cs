﻿using System;
using System.Collections.Generic;
using System.Drawing;
using GraphRepresentation;

namespace Algorithms
{
    public class GraphAlgorithm
    {
        private class Comparer : IComparer<List<int>>
        {
            public int Compare(List<int> x, List<int> y)
            {
                return x[0].CompareTo(y[0]);
            }
        }

        private static readonly Random random = new Random();

        public static int Algorithm(Vertex[] v, List<Edge>[] e1, int len)
        {
            List<List<int>> e = new List<List<int>>();
            for (int i = 0; i < len; i++)
                e.Add(new List<int>());

            int chrome;
            List<List<int>> vDegree;
            bool[] used;

            Color[] colors = new Color[len];
            for (int i = 0; i < len; i++)
                colors[i] = Color.FromArgb(random.Next(255), random.Next(255), random.Next(255));

            for (int i = 0; i < len; i++)
            {
                if (e1[i] != null)
                    for (int j = 0; j < e1[i].Count; j++)
                    {
                        int to = e1[i][j].toVertex;
                        if (!e[i].Contains(to))
                            e[i].Add(to);
                        if (!e[to].Contains(i))
                            e[to].Add(i);
                    }
            }
            vDegree = new List<List<int>>();
            for (int i = 0; i < len; i++)
            {
                vDegree.Add(new List<int>());
                vDegree[i].Add(e[i].Count);
                vDegree[i].Add(i);
            }
            Comparer comparer = new Comparer();
            used = new bool[len];
            chrome = 0;
            vDegree.Sort(comparer);
            List<int> queue = new List<int>();
            const List<int> cloneQueue = new List<int>();

            for (int i = len - 1; i >= 0; i--)
            {
                queue.Add(vDegree[i][1]);
                cloneQueue.Add(vDegree[i][1]);
            }

            while (queue.Count != 0)
            {
                for (int j = 0; j < len; j++)
                    if (v[j].color == Color.White)
                        used[j] = false;
                    else
                        used[j] = true;

                chrome++;
                int to = queue[0];
                queue.Remove(to);
                cloneQueue.Remove(to);
                v[to].color = colors[chrome];
                used[to] = true;
                for (int i = 0; i < e[to].Count; i++)
                {
                    int l = e[to][i];
                    if (!used[l])
                        cloneQueue.Remove(l);
                    used[l] = true;
                }
                while (cloneQueue.Count != 0)
                {

                    int go = cloneQueue[0];
                    cloneQueue.Remove(go);
                    if (!used[go])
                    {
                        queue.Remove(go);
                        v[go].color = colors[chrome];
                        used[go] = true;
                        for (int i = 0; i < e[go].Count; i++)
                        {
                            int l = e[go][i];
                            if (!used[l])
                                cloneQueue.Remove(i);
                            used[l] = true;
                        }
                    }
                }

                for (int i = 0; i < queue.Count; i++)
                    cloneQueue.Add(queue[i]);
            }

            return chrome;
        }
    }
}