﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GraphRepresentation;

namespace Algorithms
{
    public class GraphAlgorithm
    {
        private static readonly Random random = new Random();

        public static int Algorithm( Vertex[] v, LinkedList< Edge >[] e, int len ){
            List<List<HashSet<int>>> p = new List<List<HashSet<int>>>();
            // произведение сумм произведений
            // ( abc + cde )*...*( xyz + hz + wtf )*(a)

            for( int i = 0; i < len; i++ ){
                if( e[ i ] == null )
                    continue;
                foreach( Edge edge in e[i] ){
                    p.Add( new List< HashSet< int > > { new HashSet< int >{ i }, new HashSet< int >{ edge.toVertex } } );
                    
                }
            }

            if (p.Count == 0) //граф без ребер
            {
                Color curColor = Color.FromArgb(random.Next(255), random.Next(255), random.Next(255));
                for (int i = 0; i < len; i++)
                    v[i].color = curColor;
                return 1;
            }

            multiplicate( ref p );
            clear( ref p );

            List<HashSet<int> > F = new List< HashSet< int > >();
            for( int i = 0; i < p[ 0 ].Count; i++ ){
                HashSet<int> verts = new HashSet<int>();
                for (int j = 0; j < len; j++)
                    verts.Add(j);
                verts.ExceptWith( p[0][i] );
                F.Add(verts);
            }

            List<List<HashSet<HashSet<int> > > > result = new List< List< HashSet< HashSet<int> > > >();
            for( int i = 0; i < len; i++ ){
                List<HashSet<HashSet<int> > > summ = new List< HashSet< HashSet<int> > >();
                foreach( HashSet< int > f in F ){
                    if( f.Contains( i ) )
                        summ.Add( new HashSet< HashSet< int > >{f} );
                }
                if( summ.Count > 0 )
                 result.Add( summ );
            }

            multiplicate( ref result );

            int minLenIndex = 0;
            for( int i=1; i<result[0].Count; i++ )
                if( result[ 0 ][ i ].Count < result[ 0 ][ minLenIndex ].Count )
                    minLenIndex = i;

            for( int i = 0; i < result[ 0 ][ minLenIndex ].Count; i++ ){
                Color color = Color.FromArgb( random.Next( 255 ), random.Next( 255 ), random.Next( 255 ) );
                foreach( int vertexNum in result[0][ minLenIndex ].ElementAt( i ) ){
                    v[ vertexNum ].color = color;
                }
            }

            return result[0][ minLenIndex ].Count;
        }

        private static void multiplicate<T>( ref List< List< HashSet< T > > > p ){
            while( p.Count > 1 ){

                List<HashSet<T>> multiResult = new List<HashSet<T>>();

                for( int i = 0;  i < p[ 0 ].Count ; i++ ){

                    List<HashSet<T>> multiResultPart = new List<HashSet<T>>();

                    for( int j = 0; j < p[ 1 ].Count; j++ )
                        if( p[ 0 ][ i ].IsSubsetOf( p[ 1 ][ j ] ) ){

                            multiResultPart = new List<HashSet<T>> { new HashSet<T>(p[1][j]) };

                            for (int q = j+1; q < p[1].Count; q++)
                                if (p[1][q].IsSupersetOf(p[0][i]))
                                {
                                    multiResult.Add(p[1][q]);
                                    p[1].RemoveAt(q);
                                }

                            p[ 1 ].RemoveAt( j );
                            break;
                        }
                        else if( p[ 0 ][ i ].IsSupersetOf( p[ 1 ][ j ] ) ){

                            multiResultPart = new List<HashSet<T>> { new HashSet<T>(p[0][i]) };

                            for( int q=i+1; q<p[0].Count; q++ )
                                if( p[ 0 ][ q ].IsSupersetOf( p[ 1 ][ j ] ) ){
                                    multiResult.Add( p[ 0 ][ q ] );
                                    p[0].RemoveAt( q );
                                }

                            p[ 1 ].RemoveAt( j );
                            break;
                        }
                        else{
                            HashSet< T > multip = new HashSet< T >( p[ 0 ][ i ] );
                            multip.UnionWith( p[ 1 ][ j ] );
                            multiResultPart.Add(multip);
                        }

                    multiResult.AddRange( multiResultPart );
                }

                p[ 1 ] = multiResult;
                p.RemoveAt(0);

#if(DEBUG) 
                string[] ps = new string[p.Count];
                for (int q = 0; q < p.Count; q++)
                {
                    ps[q] += "(";
                    for (int t = 0; t < p[q].Count; t++)
                    {
                        for (int u = 0; u < p[q].ElementAt(t).Count; u++)
                            ps[q] += (p[q].ElementAt(t).ElementAt(u)).ToString();
                        if (t < p[q].Count - 1)
                            ps[q] += "+";
                    }
                    ps[q] += ")";
                }

                string[] ps1 = new string[p.Count];
                for (int q = 0; q < p.Count; q++)
                {
                    ps1[q] += "(";
                    for (int t = 0; t < p[q].Count; t++)
                    {
                        for (int u = 0; u < p[q].ElementAt(t).Count; u++)
                            ps1[q] += (p[q].ElementAt(t).ElementAt(u)).ToString();
                        if (t < p[q].Count - 1)
                            ps1[q] += "+";
                    }
                    ps1[q] += ")";
                }

                string debug = String.Join( "*", ps );
                debug += "";
#endif
            }


            
                
        }

        private static void clear(ref List<List<HashSet<int>>> p)
        {
            for (int q = 0; q < p[0][0].Count; )
            {
                int x = p[0][0].ElementAt(q);
                bool isEverywhere = true;
                for (int i = 1; i < p[0].Count; i++)
                    if (!p[0][i].Contains(x))
                        isEverywhere = false;

                if( isEverywhere ){
                    HashSet<int> addon = new HashSet< int >();
                    for( int i = 0; i < p[ 0 ].Count; i++ ){
                        addon.UnionWith( p[0][i] );
                    }
                    addon.Remove( x );
                    p[0].Add( addon );
                }

                    
                else
                    q++;
            }
        }
    }
}
