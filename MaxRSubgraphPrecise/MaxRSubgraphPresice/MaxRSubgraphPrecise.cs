﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GraphRepresentation;

namespace Algorithms
{

    public class GraphAlgorithm
    {

        private static readonly Random random = new Random();

        public static int Algorithm(Vertex[] v, LinkedList<Edge>[] e, int len){
            int colorNum = 0;

            HashSet<int>[] verts = new HashSet<int>[len];
            for (int i = 0; i < len; i++)
                verts[i] = new HashSet<int>();
            for (int i = 0; i < len; i++)
            {
                if (e[i] != null)
                    foreach (Edge edge in e[i])
                    {
                        verts[i].Add(edge.toVertex);
                        verts[edge.toVertex].Add(i);
                    }
            }

            HashSet<int> X = new HashSet< int >();
            for( int i = 0; i < len; i++ )
                X.Add( i );

            List<List<int>> S1Gj = new List<List<int>>();

            //Step1
            //каждое Sir - i-тое множество r-подграфов
            //S[r][i][индекс элемента] - вершина
            List<List<List<List<int> > > > S = new List< List< List< List< int > > > >();

            //Q - множество S, которое будем видоизменять
            List< List< List<int> > > Q = new List< List< List< int > > >();

            int r = 0;
            S.Add(  new List< List< List< int > > >(
                getIndependentSets(verts).Select( independants => new List<List<int> >{independants}) )
            );
            
            for( int i=0; i<S[r].Count; i++ )
                Q.Add( S[r][i] );

            int j = 0;

            List<int> GjVerts = X.Except(S[r][j][0]).ToList();
            if( GjVerts.Count == 0 ){
                Color curColor = Color.FromArgb(random.Next(255), random.Next(255), random.Next(255));
                foreach (int vertexes in S[r][j][0])
                {
                    v[vertexes].color = curColor;
                }
                return 1;
            }
            HashSet<int>[] GjEdges = new HashSet<int>[GjVerts.Count()];
            int t = 0;
            foreach (int vertInd in GjVerts)
            {
                GjEdges[t] = new HashSet< int >();
                foreach( int realVert in verts[vertInd] ){
                    if( GjVerts.Contains( realVert ) )
                        GjEdges[ t ].Add( GjVerts.IndexOf( realVert ) );
                }
                t++;
            }

            List<List<int>> answer = getIndependentSets(GjEdges);
            for (int i = 0; i < answer.Count; i++ )
            {
                S1Gj.Add(new List<int>());
               for( int q=0; q<answer[i].Count; q++ )
                   S1Gj[i].Add( GjVerts[ answer[i][q] ] );
            }
            
            while( true ){
                //Step2 

                if (S1Gj.Count() != 0){
                    HashSet<HashSet<int>> curS = new HashSet<HashSet<int>>(S[r][j].Select(list => new HashSet<int>(list))) { new HashSet<int>(S1Gj[0]) };

                    if (X.All(vert => curS.Any( subCurS => subCurS.Contains( vert ) )))
                    {
                        colorNum = curS.Count;
                        foreach (HashSet<int> vertexes in curS)
                        {
                            Color curColor = Color.FromArgb(random.Next(255), random.Next(255), random.Next(255));
                            bool used = false;
                            foreach( int vert in vertexes ){
                                if( v[ vert ].color == Color.White ){
                                    v[vert].color = curColor;
                                    used = true;
                                }
                            }
                            if( !used )
                                colorNum--;

                        }
                        
                        break;
                    }

                    HashSet<int> curSSet = new HashSet< int >(curS.SelectMany(x => x));

                    if( Q.Any( sublist => new HashSet< int >( sublist.SelectMany(x => x) ).IsSupersetOf( curSSet ) ) ){
                        S1Gj.RemoveAt(0);
                        continue;
                    }

                    for( int i = 0; i < Q.Count; ){
                        if( new HashSet< int >( Q[ i ].SelectMany( x => x ) ).IsSubsetOf( curSSet ) )
                            Q.RemoveAt( i );
                        else
                            i++;
                    }

                    Q.Add(curS.Select( set => set.ToList()  ).ToList());

                    //S.Add( S[r][j].Concat( S1Gj ) );

                    S1Gj.RemoveAt(0);
                }
                else
                {
                    if( j < Q.Count - 1 )
                        j++;
                    else{
                        j = 0;
                        r++;
                        S.Add( Q );

                        GjVerts = X.Except( S[ r ][ j ].SelectMany( x => x ) ).ToList();
                        GjEdges = new HashSet< int >[GjVerts.Count()];
                        t = 0;
                        foreach( int vertInd in GjVerts ){
                            GjEdges[ t ] = new HashSet< int >();
                            foreach( int realVert in verts[ vertInd ] ){
                                if( GjVerts.Contains( realVert ) )
                                    GjEdges[ t ].Add( GjVerts.IndexOf( realVert ) );
                            }
                            t++;
                        }

                        answer = getIndependentSets( GjEdges );
                        for( int i = 0; i < answer.Count; i++ ){
                            S1Gj.Add( new List< int >() );
                            for( int q = 0; q < answer[ i ].Count; q++ )
                                S1Gj[ i ].Add( GjVerts[ answer[ i ][ q ] ] );
                        }

                    }

                }
            }
            
            return colorNum;
        }

        private static List<List<int>> getIndependentSets(HashSet<int>[] e)
        {
            List<List<int>> result = new List<List<int>>();

            if (e.Length == 1)
            {
                if( e[0] != null )
                    result.Add(e[0].ToList());
                else
                    result.Add(new List<int>{ 0 });
                result[0].Add( 0 );
                return result;
            }

            HashSet<int> S = new HashSet<int>();
            HashSet<int>[] usedQ = new HashSet<int>[e.Length+1];
            HashSet<int>[] unusedQ = new HashSet<int>[e.Length+1];

            usedQ[0] = new HashSet<int>();
            unusedQ[0] = new HashSet<int>();

            for (int i = 0; i < e.Length; i++)
                 unusedQ[0].Add(i);

            int k = 0;
            int[] x = new int[e.Length];

            x[k] = unusedQ[k].First();
            S.Add(x[k]);
            k++;

            unusedQ[k] = new HashSet<int>(unusedQ[k - 1]);
            unusedQ[k].RemoveWhere(vert => e[x[k - 1]].Contains(vert));
            unusedQ[k].Remove(x[k - 1]);
            usedQ[k] = new HashSet<int>(usedQ[k - 1]);
            usedQ[k].RemoveWhere(vert => e[x[k - 1]].Contains(vert));

            while (k > 0 || unusedQ[0].Count != 0)
            {


                bool unreachableMax = false;
                foreach (int xk in usedQ[k])
                {
                    if (!unusedQ[k].Intersect(e[xk]).Any())
                        unreachableMax = true;
                }

                if (!unreachableMax)
                    if (unusedQ[k].Count == 0)
                    {
                        if (usedQ[k].Count == 0)
                            result.Add(S.ToList());
                    }
                    else
                    {
                        x[k] = unusedQ[k].First();
                        S.Add(x[k]);
                        k++;

                        unusedQ[k] = new HashSet<int>(unusedQ[k - 1]);
                        unusedQ[k].RemoveWhere(vert => e[x[k - 1]].Contains(vert));
                        unusedQ[k].Remove(x[k - 1]);
                        usedQ[k] = new HashSet<int>(usedQ[k - 1]);
                        usedQ[k].RemoveWhere(vert => e[x[k - 1]].Contains(vert));

                        continue;
                    }

                k--;
                if (k < 0)
                {
                    k++;
                    x[k] = unusedQ[k].First();
                    S.Add(x[k]);
                }
                S.Remove(x[k]);
                usedQ[k].Add(x[k]);
                unusedQ[k].Remove(x[k]);
            }

            return result;
        }
    }
}

