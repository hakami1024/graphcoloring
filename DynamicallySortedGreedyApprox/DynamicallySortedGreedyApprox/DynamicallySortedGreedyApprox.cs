﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GraphRepresentation;

namespace Algorithms
{

    public class GraphAlgorithm
    {

        private static readonly Random r = new Random();


        public static int Algorithm(Vertex[] v, LinkedList<Edge>[] e, int len)
        {
            List<Tuple<HashSet<int>, int>> edges = new List<Tuple<HashSet<int>, int>>();
            

            for( int i = 0; i < len; i++ )
                edges.Add(new Tuple< HashSet< int >, int >( new HashSet< int >(), i) );
            
            for (int i = 0; i < len; i++)
            {
                if( e[i] != null)
                foreach( Edge edg in e[i] ){
                    edges[ i ].Item1.Add( edg.toVertex );
                    edges[ edg.toVertex ].Item1.Add( i );
                }
            }


            int colorNum = 0;
            while( edges.Count > 0 ){

                edges.Sort((e1, e2) => -1*e1.Item1.Count.CompareTo(e2.Item1.Count));

                colorNum++;
                Color curColor = Color.FromArgb( r.Next( 255 ), r.Next( 255 ), r.Next( 255 ) );
                v[ edges[ 0 ].Item2 ].color = curColor;


                for( int i=0; i<edges.Count; i++ )
                    if (edges[i].Item1.All(edge => v[edge].color != curColor))
                        v[  edges[ i ].Item2 ].color = curColor;
                    

                for( int i=1; i<edges.Count; ){
                    if( v[ edges[ i ].Item2 ].color == curColor ){
                        for( int j = 1; j < edges.Count; j++ )
                            edges[ j ].Item1.Remove( edges[ i ].Item2 );
                        edges.RemoveAt( i );
                    }
                    else
                        i++;
                }

                for (int i=0; i<edges.Count; i++)
                {
                    edges[i].Item1.Remove(edges[0].Item2);
                }
                edges.RemoveAt(0);

            }
            
            return colorNum;
        }
    }
}

