#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

string to_string( int x ){
    string s = "";
    while( x>0 ){
        char s1 = '0' + x%10;
        s =  s1 + s;
        x /= 10;
    }
    return s;
}
void makeFile( const char* fileName, int N ){
    FILE *pFile = fopen ( fileName, "w" );
	fprintf( pFile, "%d\n", N );

    int sideLen = 1;//0;
    int lines = N / sideLen;


    for( int i=0, q=0; i<lines; i++ )
        for( int j=0; j<sideLen; j++, q++ )
            fprintf( pFile, "v%d\n%d\n%d\n", q, i, j );

    int q=0;
    for( int i=0; i<N; i++ )
        for( int j=i+1; j<N; j++ )
            if( (i+j)%2 == 0 )
                q++;

    fprintf( pFile, "\n%d\n", q ); // (int)(((double)N+0.5)/2)*(N-1)

    for( int i=0; i<N; i++ )
        for( int j=i+1; j<N; j++ )
            if( (i+j)%2 == 0 )
                fprintf( pFile, "v%d\nv%d\n0\n", i, j );
}

int main()
{
    string dir = "C:\\Users\\������\\Desktop\\graph\\half\\";

    FILE *pFile = fopen ( "HalfGraphDir.txt","w" );

    for( int i=1; i<10; i++ ){
        string name = "Graph_" + to_string(i) + ".txt";
        makeFile( name.c_str(), i );
        fprintf( pFile, (dir+name+"\n").c_str() );
    }

    for( int i=10; i<100; i+=10 ){
        string name = "Graph_" + to_string(i) + ".txt";
        makeFile( name.c_str(), i );
        fprintf( pFile, (dir+name+"\n").c_str() );
    }
}

