# Библиотеки алгоритмов приближенной и точной раскраски графов #

Реализованы различные алгоритмы для вершинной раскраски графа.

Для передачи библиотеке графа, представьте граф списком вершин, используя эту [библиотеку](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/GraphAlgorithmsTester/GraphAlgorithmsTester/bin/Release/GraphRepresentation.dll?at=master).

### Библиотеки точной раскраски: ###
* [На основе алгоритма Магу-Вейсмана](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/MaguVeismanPrecise/?at=master) 
* [По алгоритму поиска максимальных r-подграфов](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/MaxRSubgraphPrecise/?at=master)

### Библиотеки приближенной раскраски: ###
* [Простейшая](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/SimpleGreedyApprox/?at=master)
* [Со статической сортировкой](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/SortedGreedyApprox/?at=master)
* [С динамической сортировкой](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/DynamicallySortedGreedyApprox/?at=master)
* [Эвристика hakami1024, на основе поиска независимых подмножеств](https://bitbucket.org/hakami1024/graphcoloring/src/e3589b8612965d3091b4b1f2439df96d9b0adc59/ComplementGraphApprox/?at=master)